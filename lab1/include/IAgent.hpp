#pragma once

#include "World.hpp"
#include <list>

class IAgent {
public:
  explicit IAgent(World &world);
  virtual void iteration() = 0;
  virtual long long learn() = 0;
  WorldArr<char> getPolicy() const { return policy; };
  virtual void calculatePolicy() = 0;
  virtual ~IAgent() = default;
  virtual WorldArr<std::list<double>> getHistory() const;
  const WorldArr<double> &getUtilities() const { return utilities; }
  WorldArr<double> &getUtilities() { return utilities; }
  const World &getWorld() { return world; }
  virtual std::string details() const;

protected:
  static std::pair<int, int> fixPosition(std::pair<int, int> currentPosition,
                                         std::pair<int, int> nextPosition,
                                         WorldArr<World::State> states);

  World &world;
  double aware;
  double gamma;
  double r;
  double pForward;
  double pRight;
  double pLeft;
  double pBack;
  WorldArr<std::list<double>> history;
  WorldArr<double> utilities;
  WorldArr<char> policy;
};