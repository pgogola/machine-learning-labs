import src.classifier as clf
from src import util
import numpy as np
import argparse
from sklearn import svm


def generator_k_fold(data, k):
    step = int(len(data)/k)
    for i in range(0, len(data), step):
        yield (data[:i]+data[i+step:], data[i:i+step])


def dfm_basic_classifier_predict(classifier, dataset, model_path):
    labels = {-1: 0,
              0: 1,
              1: 2}
    classifier.load(model_path)
    dataset, _ = util.data_to_dfm(
        dataset, classifier.additional_data["word_encoding"])
    dataset = util.shuffle_data(dataset)
    all_samples = 0
    correct_classified_samples = 0
    tab = np.zeros((3, 3))
    x_test, y_test = util.data_for_clf_np(dataset)
    predicted = classifier.predict(x_test)
    correct = 0
    for i, p in enumerate(predicted):
        tab[labels[p], labels[y_test[i]]] += 1
        if p == y_test[i]:
            correct += 1
        print(y_test[i], p, end=' ')
    correct_classified_samples += correct
    all_samples += len(predicted)


def idfm_basic_classifier_predict(classifier, dataset, model_path):
    labels = {-1: 0,
              0: 1,
              1: 2}
    classifier.load(model_path)
    dataset, _ = util.data_to_idfm(
        dataset, classifier.additional_data["word_encoding"])
    dataset = util.shuffle_data(dataset)
    all_samples = 0
    correct_classified_samples = 0
    tab = np.zeros((3, 3))
    x_test, y_test = util.data_for_clf_np(dataset)
    predicted = classifier.predict(x_test)
    correct = 0
    for i, p in enumerate(predicted):
        tab[labels[p], labels[y_test[i]]] += 1
        if p == y_test[i]:
            correct += 1
        print(y_test[i], p, end=' ')
    correct_classified_samples += correct
    all_samples += len(predicted)


def nn_classifier_predict(classifier, dataset, model_path):
    NGRAM = 2
    data = []
    labels = {-1: 0,
              0: 1,
              1: 2}
    for c, s in dataset:
        data.append((labels[c], util.string_to_words(s) +
                     util.get_ngrams(s, NGRAM)))
    classifier.load(model_path)
    word_encoding = classifier.additional_data["word_encoding"]
    encoded_data = []
    for c, words in data:
        encoded_data.append((c, [word_encoding[w]
                                 for w in words if w in word_encoding]))

    all_samples = 0
    correct_classified_samples = 0
    tab = np.zeros((3, 3))
    test = encoded_data
    predicted = classifier.predict(test)
    correct = 0
    for i, p in enumerate(predicted):
        tab[p, test[i][0]] += 1
        if p == test[i][0]:
            correct += 1
        print(test[i][0], p, end=' ')
    correct_classified_samples += correct
    all_samples += len(predicted)


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("--path", type=str, dest="model_path",
                        action="store", required=False,
                        help="path to the model")
    parser.add_argument("-k", type=int, dest="kfold",
                        action="store", required=False,
                        default=20,
                        help="crossvalidation k parameter")
    required_args = parser.add_argument_group('required arguments')
    required_args.add_argument("--clf", type=str, dest="classifier",
                               action="store", default=False,
                               choices=['zeror', 'gnb', 'gnbova', 'mnb', 'mnbova',
                                        'svm', "svmova", "nn"],
                               required=True, help="select classifier:\n" +
                               "--clf zeror     - zeroR classifier\n" +
                               "--clf gnb       - gaussian naive bayes classifier\n" +
                               "--clf gnbova    - gaussian naive bayes one vs all classifier\n" +
                               "--clf mnb       - multinomial naive bayes classifier\n" +
                               "--clf mnbova    - multinomial naive bayes one vs all classifier\n" +
                               "--clf svm       - support vector machine classifier\n" +
                               "--clf svmova    - support vector machine one vs all classifier\n" +
                               "--clf nn        - neural network classifier\n")
    required_args.add_argument("--mode", type=str, dest="mode",
                               action="store", default=False,
                               choices=["train", "predict", "crossval"],
                               required=True, help="select mode of working program:\n" +
                               "--mode train: classifier will be learned from given dataset\n" +
                               "--mode predict: classifier will predict class of articles and show basic measures\n" +
                               "--mode crossval: classifier be trained and tested in cross validation\n")
    required_args.add_argument("--data", type=str, dest="data",
                               action="store", default=False,
                               required=True, help="path to data in txt file\n" +
                               "when --mode train is used, then dataset is used to train selected model\n" +
                               "when --mode predict is used, then dataset is used to predict data\n")
    classifier_args = parser.add_argument_group(
        'classifier optional arguments')
    classifier_args.add_argument("--svmC", type=int, dest="svmC",
                                 action="store", default=1,
                                 required=False, help="svm C parameter\n")
    classifier_args.add_argument("--svmKernel", type=str, dest="svmKernel",
                                 action="store", default="rbf",
                                 choices=['rbf', 'linear', 'poly'],
                                 required=False, help="svm kernel parameter\n")
    classifier_args.add_argument("--svmDegree", type=int, dest="svmDegree",
                                 action="store", default=3,
                                 required=False, help="svm degree parameter\n")
    classifier_args.add_argument("--svmGamma", type=str, dest="svmGamma",
                                 action="store", default="scale",
                                 required=False, help="svm gamma parameter\n")
    classifier_args.add_argument("--nnBatchSize", type=int, dest="nnBatchSize",
                                 action="store", default=50,
                                 required=False, help="neural network batch size parameter\n")
    classifier_args.add_argument("--nnLr", type=float, dest="nnLr",
                                 action="store", default=2.0,
                                 required=False, help="neural network initial learning rate parameter\n")
    classifier_args.add_argument("--output_dim", type=float, dest="output_dim",
                                 action="store", default=3,
                                 required=False, help="neural network output dimension (number of classes)\n")
    classifier_args.add_argument("--embed_dim", type=int, dest="embed_dim",
                                 action="store", default=4,
                                 required=False, help="neural network embedded layer dimension\n")
    classifier_args.add_argument("--epochs", type=int, dest="epochs",
                                 action="store", default=50,
                                 required=False, help="number of epoch in neural network train process\n")
    args = parser.parse_args()
    alternative_classifier_options = {
        "svmC": args.svmC,
        "svmKernel": args.svmKernel,
        "svmDegree": args.svmDegree,
        "svmGamma": args.svmGamma,
        "nnLr": args.nnLr,
        "nnBatchSize": args.nnBatchSize,
        "output_dim": args.output_dim,
        "embed_dim": args.embed_dim,
        "epochs": args.epochs
    }

    idfm_basic_classifiers = set(["zeror", "gnb", "gnbova", "svm", "svmova"])
    dfm_basic_classifiers = set(["mnb", "mnbova"])
    nn_classifiers = ["nn"]
    dataset = util.txt_data_loader(args.data)
    if args.classifier in idfm_basic_classifiers:
        bc_constr = {
            "zeror": "clf.ZeroR",
            "gnb": "clf.GaussianNaiveBayes",
            "gnbova": "clf.GaussianNaiveBayesOneVsAll",
            "svm": "clf.SupportVectorMachine",
            "svmova": "clf.SupportVectorMachineOneVsAll"
        }
        classifier = eval(bc_constr[args.classifier])(
            **alternative_classifier_options)
        if args.mode == "predict":
            idfm_basic_classifier_predict(classifier,
                                          dataset,
                                          args.model_path)

    if args.classifier in dfm_basic_classifiers:
        bc_constr = {
            "mnb": "clf.MultinomialNaiveBayes",
            "mnbova": "clf.MultinomialNaiveBayesOneVsAll",
        }
        classifier = eval(bc_constr[args.classifier])(
            **alternative_classifier_options)
        if args.mode == "predict":
            dfm_basic_classifier_predict(classifier,
                                         dataset,
                                         args.model_path)

    elif args.classifier in nn_classifiers:
        nnc_constr = {
            "nn": "clf.NNClassifier"
        }
        words_set = util.get_all_words_set(dataset)
        NGRAM = 2
        data = []
        for c, s in dataset:
            data.append(util.string_to_words(s) +
                        util.get_ngrams(s, NGRAM))
        vocab = set()
        for words in data:
            for w in words:
                vocab.add(w)
        classifier = eval(nnc_constr[args.classifier])(
            len(vocab), **alternative_classifier_options)
        if args.mode == "predict":
            nn_classifier_predict(classifier,
                                  dataset,
                                  args.model_path)


if __name__ == "__main__":
    main()
