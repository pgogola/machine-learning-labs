# Klasyfikator tekstów

## Wymagania:

1. Python 3.6
2. pip

## Instalacja

Konfiguracja środowiska i instalacja wymaganych pakietów realizowana jest poprzez uruchomienie skryptu **install.sh** w katalogu gównym projektu. Po jego uruchomieniu tworzone jest lokalne środowisko Python ([Python Documentation: 12. Virtual Environments and Packages](https://docs.python.org/3/tutorial/venv.html)), a następnie instalowane pakiety na podstawie pliku **requirements.txt**.

## Uruchomienie programu

Program posiada interfejs konsolowy: dane wejściowe, tryb pracy programu, dane konfiguracyjne podawane są w postaci listy argumentów wiersza poleceń. W celu wyświetlenia pełnej listy opcji należy uruchomić program z flagą **--help**:

```bash
python ./main --help
```

## Sktypt uruchomieniowy Runme.sh

W katalogu ***Runme*** znajduje się skrypt realizujący predykcję klas dla artykułów znajdujących się w katalogu ***Runme*** (jeden artykuł w jednym pliku z rozszerzeniem .txt). Z wykorzystaniem klasyfikatorów zapisanych w katalogu ***resources/pretrained_models/***.

Program realizuje predykcje na podstawie wszystkich opracowych algorytmów wyświetlając listę w postaci **nr klasa_oryginalna klasa_obliczona nazwa_pliku**. Na końcu wyświetlana jest dokładność predykcji oraz macierz pomyłek dla każdego klasyfikatora (wiersz: rzeczywista klasa artykułu, kolumna: klasyfikacja).

## Dodatkowe sktypty uruchomieniowe

Przygotowano 3 skrypty:

1. **crossval.sh** uruchamiający walidację krzyżową (k=10) dla każdego klasyfikatora ***CLF*** dla zbioru danych zawartego w pliku **resources/dataset.txt** za pomocą polecenia:

    ```bash
    python ./main.py --mode crossval -k 10 --data ./resources/dataset.txt --path ./tmp/model.bin --clf $CLF
    ```

2. **train_pretrainmodels.sh** generującego klasyfikatory ***CLF*** dla zbioru danych zawartego w pliku **resources/train.txt** za pomocą polecenia:

    ```bash
    python ./main.py --mode train --data ./resources/train.txt --path ./resources/pretrained_models/$CLF.bin --clf $CLF --epochs 100
    ```

    Klasyfikatory zapisywane są w katalogu **resources/pretrained_models/**

3. **test_pretrainmodels.sh** uruchamiający predykcje klasy za pomocą  klasyfikatora ***CLF*** dla zbioru danych zawartego w pliku **resources/test.txt** za pomocą polecenia:

    ```bash
    python ./main.py --mode predict --data ./resources/test.txt --path ./resources/pretrained_models/$CLF.bin --clf $CLF --epochs 100
    ```

    Klasyfikatory łądowane są z katalogu **resources/pretrained_models/**

## Informacja o jakości klasyfikacji

Program w przypadku walidacji krzyżowej i predykcji klas na zadanym zbiorze wyświetla tablice pomyłek (wiersz: klasa uzyskana przez klasyfikator, kolumna: rzeczywista klasa).

## Kodowanie wyrazów

W sytuacji uruchomienia programu w trybie klasyfikacji z wykorzystaniem nauczonych modeli, gdy w zbiorze testowym nie wystąpiło jakieś słowo, jest one pomijane i nie bierze udziału jako cecha tekstu.

## Oznaczenia klasyfikatorów

1. zeror - Klasyfikator zeror

2. gnb - Gausowski klasyfikator Naiwnego Bayesa 

3. gnbova - Gausowski klasyfikator Naiwnego Bayesa One-vs-All 

4. mnb - Wielomianowy klasyfikator Naiwnego Bayesa 

5. mnbova - Wielomianowy klasyfikator Naiwnego Bayesa One-vs-All 

6. svm - Maszyna wektorów nośnych

7. svmova - Maszyna wektorów nośnych One-vs-All

8. nn - Klasyfikator sieci neuronowych