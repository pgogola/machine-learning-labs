import os
from torchtext.datasets import text_classification
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.multiclass import OneVsRestClassifier
from sklearn import svm
import numpy as np
from torch.utils.data import DataLoader
from torch import nn, optim
import torch
from joblib import dump, load
from abc import ABC, abstractmethod


class ClassifierI(ABC):
    def __init__(self):
        super().__init__()
        self.additional_data = {}

    @abstractmethod
    def train(self, samples, labels):
        pass

    @abstractmethod
    def predict(self, samples):
        pass

    @abstractmethod
    def save(self, filename):
        pass

    @abstractmethod
    def load(self, filename):
        pass


class BasicClassifierI(ClassifierI):
    def __init__(self):
        super().__init__()

    def save(self, filename):
        dump((self.clf, self.additional_data), filename)

    def load(self, filename):
        (self.clf, self.additional_data) = load(filename)


class NNClassifierI(ClassifierI):
    def __init__(self):
        super().__init__()

    def save(self, filename):
        torch.save((self.clf, self.additional_data), filename)

    def load(self, filename):
        self.clf, self.additional_data = torch.load(filename)
        self.clf.eval()


class ZeroR(BasicClassifierI):
    def __init__(self, **kwargs):
        super().__init__()
        self.clf = None

    def train(self, samples, labels):
        self.clf = max(set(labels), key=labels.tolist().count)

    def predict(self, samples):
        return np.full(samples.shape[0], self.clf)


class GaussianNaiveBayes(BasicClassifierI):
    def __init__(self, **kwargs):
        super().__init__()
        self.clf = GaussianNB(priors=[1/3, 1/3, 1/3])

    def train(self, samples, labels):
        self.clf.fit(samples, labels)

    def predict(self, samples):
        return self.clf.predict(samples)


class GaussianNaiveBayesOneVsAll(BasicClassifierI):
    def __init__(self, **kwargs):
        super().__init__()
        self.clf = OneVsRestClassifier(GaussianNB())

    def train(self, samples, labels):
        self.clf.fit(samples, labels)

    def predict(self, samples):
        return self.clf.predict(samples)


class MultinomialNaiveBayes(BasicClassifierI):
    def __init__(self, **kwargs):
        super().__init__()
        self.clf = MultinomialNB()

    def train(self, samples, labels):
        self.clf.fit(samples, labels)

    def predict(self, samples):
        return self.clf.predict(samples)


class MultinomialNaiveBayesOneVsAll(BasicClassifierI):
    def __init__(self, **kwargs):
        super().__init__()
        self.clf = OneVsRestClassifier(MultinomialNB())

    def train(self, samples, labels):
        self.clf.fit(samples, labels)

    def predict(self, samples):
        return self.clf.predict(samples)


class SupportVectorMachineOneVsAll(BasicClassifierI):
    def __init__(self, **kwargs):
        super().__init__()
        self.C = kwargs["svmC"]
        self.kernel = kwargs["svmKernel"]
        self.degree = kwargs["svmDegree"]
        try:
            self.gamma = float(kwargs["svmGamma"])
        except ValueError:
            self.gamma = kwargs["svmGamma"]
        self.clf = OneVsRestClassifier(svm.SVC(C=self.C, kernel=self.kernel,
                                               degree=self.degree, gamma=self.gamma))

    def train(self, samples, labels):
        self.clf.fit(samples, labels)

    def predict(self, samples):
        return self.clf.predict(samples)


class SupportVectorMachine(BasicClassifierI):
    def __init__(self, **kwargs):
        super().__init__()
        self.C = kwargs["svmC"]
        self.kernel = kwargs["svmKernel"]
        self.degree = kwargs["svmDegree"]
        try:
            self.gamma = float(kwargs["svmGamma"])
        except ValueError:
            self.gamma = kwargs["svmGamma"]
        self.clf = svm.SVC(C=self.C, kernel=self.kernel,
                           degree=self.degree, gamma=self.gamma)

    def train(self, samples, labels):
        self.clf.fit(samples, labels)

    def predict(self, samples):
        return self.clf.predict(samples)


class NNClassifier(NNClassifierI):
    class TextClassifier(nn.Module):

        @classmethod
        def generate_train_batch(cls, batch):
            label = torch.tensor([entry[0] for entry in batch])
            text = [torch.LongTensor(entry[1]) for entry in batch]
            offsets = [0] + [len(entry) for entry in text]
            offsets = torch.tensor(offsets[:-1]).cumsum(dim=0)
            text = torch.cat(text)
            return text, offsets, label

        @classmethod
        def generate_predict_batch(cls, batch):
            text = [torch.LongTensor(entry[1]) for entry in batch]
            offsets = [0] + [len(entry) for entry in text]
            offsets = torch.tensor(offsets[:-1]).cumsum(dim=0)
            text = torch.cat(text)
            return text, offsets

        def __init__(self, vocab_size, embed_dim, output_dim):
            super(NNClassifier.TextClassifier, self).__init__()
            self.embedding = nn.EmbeddingBag(
                vocab_size, embed_dim, sparse=True)
            self.dropout = nn.Dropout(p=0.4)
            self.fc1 = nn.Linear(embed_dim, output_dim)
            self.init_weights()

        def init_weights(self):
            initrange = 0.5
            self.embedding.weight.data.uniform_(-initrange, initrange)
            self.fc1.weight.data.uniform_(-initrange, initrange)
            self.fc1.bias.data.zero_()

        def forward(self, x, offsets):
            x = self.embedding(x, offsets)
            x = self.dropout(x)
            x = self.fc1(x)
            return x

    def __init__(self, vocab_size, **kwargs):
        super().__init__()
        self.clf = NNClassifier.TextClassifier(
            vocab_size, kwargs["embed_dim"], kwargs["output_dim"])
        self.batch_size = kwargs["nnBatchSize"]
        self.epochs = kwargs["epochs"]
        self.init_lr = kwargs["nnLr"]

    def train(self, samples):
        self.clf.init_weights()
        self.clf.train()
        train_loss = 0
        train_acc = 0
        criterion = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(self.clf.parameters(), lr=self.init_lr)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 5, gamma=0.99)
        for e in range(self.epochs):
            train_loss = 0
            train_acc = 0
            data = DataLoader(samples, batch_size=self.batch_size, shuffle=True,
                              collate_fn=NNClassifier.TextClassifier.generate_train_batch)
            for i, (text, offsets, cls) in enumerate(data):
                optimizer.zero_grad()
                output = self.clf(text, offsets)
                loss = criterion(output, cls)
                train_loss += loss.item()
                loss.backward()
                optimizer.step()
                train_acc += (output.argmax(1) == cls).sum().item()
            print(train_loss / len(samples),
                  train_acc / len(samples))
            scheduler.step()

    def predict(self, samples):
        self.clf.eval()
        train_acc = 0
        data = DataLoader(samples, batch_size=self.batch_size,
                          collate_fn=NNClassifier.TextClassifier.generate_predict_batch)
        predicted = []
        for i, (text, offsets) in enumerate(data):
            output = self.clf(text, offsets)
            predicted = predicted + output.argmax(1).detach().numpy().tolist()

        return predicted
