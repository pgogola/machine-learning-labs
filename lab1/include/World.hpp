#pragma once

#include <algorithm>
#include <iostream>
#include <string>

template <typename U> class WorldArr;

template <typename U>
std::ofstream &operator<<(std::ofstream &os, const WorldArr<U> &arr);

template <typename T> class WorldArr {
public:
  explicit WorldArr(int rows, int cols);
  WorldArr(const WorldArr &other);
  WorldArr(WorldArr &&other);
  ~WorldArr();

  WorldArr &operator=(const WorldArr &other);
  const T &operator()(int row, int col) const;
  T &operator()(int row, int col);
  template <typename U>
  friend std::ostream &operator<<(std::ostream &os, const WorldArr<U> &arr);
  int getCols() const { return this->cols; }
  int getRows() const { return this->rows; }

private:
  T **arr;
  int rows;
  int cols;
};

template <typename T>
WorldArr<T>::WorldArr(int rows, int cols)
    : arr{nullptr}, rows{rows}, cols{cols} {
  this->arr = new T *[rows];
  for (int i = 0; i < rows; i++) {
    this->arr[i] = new T[cols];
  }
}

template <typename T>
WorldArr<T>::WorldArr(const WorldArr &other)
    : rows{other.rows}, cols{other.cols} {
  this->arr = new T *[other.rows];
  for (int i = 0; i < this->rows; i++) {
    this->arr[i] = new T[this->cols];
    for (int j = 0; j < this->cols; j++) {
      this->arr[i][j] = other.arr[i][j];
    }
  }
}

template <typename T>
WorldArr<T>::WorldArr(WorldArr &&other)
    : arr{nullptr}, rows{other.rows}, cols{other.cols} {
  other.rows = 0;
  other.cols = 0;
  std::swap(arr, other.arr);
}

template <typename T> WorldArr<T>::~WorldArr() {
  if (arr) {
    for (int i = 0; i < this->rows; i++) {
      if (arr[i]) {
        delete[] arr[i];
        arr[i] = nullptr;
      }
    }
    delete[] arr;
  }
}

template <typename T>
WorldArr<T> &WorldArr<T>::operator=(const WorldArr &other) {
  rows = other.rows;
  cols = other.cols;
  this->arr = new T *[other.rows];
  for (int i = 0; i < this->rows; i++) {
    this->arr[i] = new T[this->cols];
    for (int j = 0; j < this->cols; j++) {
      this->arr[i][j] = other.arr[i][j];
    }
  }
  return *this;
}

template <typename T> const T &WorldArr<T>::operator()(int row, int col) const {
  return arr[row][col];
}

template <typename T> T &WorldArr<T>::operator()(int row, int col) {
  return arr[row][col];
}

template <typename U>
std::ostream &operator<<(std::ostream &os, const WorldArr<U> &world) {
  for (int i = 0; i < world.rows; i++) {
    for (int j = 0; j < world.cols; j++) {
      os << world.arr[i][j] << " ";
    }
    os << std::endl;
  }
  return os;
}

class World {
public:
  enum Direction { UP, DOWN, RIGHT, LEFT, UNDEF };
  enum State : char { B = 'B', S = 'S', T = 'T', F = 'F', N = 'N' };
  static constexpr char directionIntChar[5]{'^', 'v', '>', '<', '.'};

  static World readWorldFromFile(const std::string &filename);

  template <typename T>
  static void printWorldMap(const T **arr, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        std::cout << arr[i][j] << " ";
      }
      std::cout << std::endl;
    }
  }

  /*
    N - map height (in vertical)
    M - map width (in horizontal)
  */
  explicit World(int rows, int cols);
  World(const World &other);
  World(World &&other);

  ~World();

  int getCols() { return this->cols; }
  int getRows() { return this->rows; }
  double getP3() { return this->p3; }
  void setP3(double p3) { this->p3 = p3; }
  double getR() { return this->r; }
  void setR(double r) {
    this->r = r;
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        scores(i, j) = r;
      }
    }
  }
  double getGamma() { return this->gamma; }
  void setGamma(double gamma) { this->gamma = gamma; }
  int getN_t() { return this->n_t; }
  void setN_t(int n_t) { this->n_t = n_t; }
  int getN_b() { return this->n_b; }
  void setN_b(int n_b) { this->n_b = n_b; }
  int getN_f() { return this->n_f; }
  void setN_f(int n_f) { this->n_f = n_f; }
  double getP1() { return this->p1; }
  void setP1(double p1) { this->p1 = p1; }
  double getP2() { return this->p2; }
  void setP2(double p2) { this->p2 = p2; }
  const std::pair<int, int> &getStartPosition() const;
  void setStartPosition(int row, int col);
  const WorldArr<double> &getScores() const;
  WorldArr<double> &getScores();
  const WorldArr<State> &getStates() const;
  WorldArr<State> &getStates();
  const WorldArr<Direction> &getDirections() const;
  WorldArr<Direction> &getDirections();

private:
  int rows;
  int cols;
  double p1;
  double p2;
  double p3;
  double r;
  double gamma;
  int n_t;
  int n_b;
  int n_f;
  WorldArr<double> scores;
  WorldArr<Direction> directions;
  WorldArr<State> states;
  std::pair<int, int> startPosition;
};

std::ostream &operator<<(std::ostream &os, const World::State &obj);
std::ostream &operator<<(std::ostream &os, const World::Direction &obj);