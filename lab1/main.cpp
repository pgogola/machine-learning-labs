#include "include/MdpAgent.hpp"
#include "include/RlAgent.hpp"
#include "include/World.hpp"
#include "include/plot.hpp"
#include <iostream>
#include <memory>

void mdpAgentRun() {
  std::string exampleFileNames[]{
      "exampleRuselNorvig.txt", // ex. 1, p1=0.8, p2,p3=0.1, y=1, r=-0.04
      "exampleRys2.txt",        // ex. 2. p1=0.8, p2,p3=0.1, y=0.99, b=-20, r=-1
      "exampleRys2otherGamma.txt", // ex. 2. p1=0.8, p2,p3=0.1, y=0.80, b=-20,
                                   // r=-1
      "exampleRys2otherP.txt", // ex. 2. p1=0.5, p2=0.5, p3=0.0, y=0.99, b=-20,
                               // r=-1
      "exampleRys2otherScore.txt" // ex. 2. p1=0.8, p2,p3=0.1, y=0.99, b=-50,
                                  // t=50 r=-0.5
  };
  for (const auto &efn : exampleFileNames) {
    std::cout << efn << std::endl;
    World world = World::readWorldFromFile("../resources/" + efn);
    auto agentMdp = std::make_unique<MdpAgent>(world);
    WorldArr<double> prevUtilities = agentMdp->getUtilities();

    auto iterations = agentMdp->learn();
    std::cout << "iterations " << iterations << std::endl;
    agentMdp->calculatePolicy();
    saveGraphUtilityHistory(agentMdp->getHistory(), "../resultPlots/mdp" + efn);
    plotGraphFromFile("../resultPlots/mdp" + efn);
    std::cout << agentMdp->getWorld().getStates() << std::endl;
    std::cout << agentMdp->details() << std::endl;
  }
}

void rlAgentRun() {
  std::string exampleFileNames[]{
      // "exampleRuselNorvig.txt",
      "exampleRys2.txt",
      // "exampleRys2otherGamma.txt",
      // "exampleRys2otherP.txt", "exampleRys2otherScore.txt"
  };
  double epsilons[]{0.05, 0.2};
  const long long iterationsArr[]{10000, 50000, 100000, 500000, 1000000};
  for (const auto epsilon : epsilons) {
    for (const auto &efn : exampleFileNames) {
      for (unsigned long iterations : iterationsArr) {
        std::cout << efn << " iterations " << iterations << std::endl;
        std::cout << efn << " epsilon " << epsilon << std::endl;
        World world = World::readWorldFromFile("../resources/" + efn);
        auto agentRl = std::make_unique<RlAgent>(world, epsilon);
        FileManager fileManager("../resultPlots/rl" +
                                std::to_string(iterations) + "_" +
                                std::to_string(epsilon) + efn);
        fileManager.open();
        fileManager.prepare(agentRl->getUtilities());
        for (unsigned long it = 0; it < iterations; it++) {
          agentRl->learn();
          agentRl->updateUtility();
          fileManager.writeData(agentRl->getUtilities());
        }
        fileManager.close();
        plotGraphFromFile("../resultPlots/rl" + std::to_string(iterations) +
                          "_" + std::to_string(epsilon) + efn);
        agentRl->calculatePolicy();
        std::cout << agentRl->getWorld().getStates() << std::endl;
        std::cout << agentRl->details() << std::endl;
      }
    }
  }
}

int main() {
  try {
    mdpAgentRun();
    rlAgentRun();
  } catch (const char *er) {
    std::cout << er << std::endl;
  } catch (...) {
    std::cout << "unexpected exception" << std::endl;
  }
  return 0;
}