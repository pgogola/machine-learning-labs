#!/bin/bash

source ./../env/bin/activate

for CLF in "zeror" "gnb" "gnbova" "mnb" "mnbova" "svm" "svmova" "nn"; do
    correct=0
    declare -A matrix
    num_rows=3
    num_columns=3

    for ((i=1;i<=num_rows;i++)) do
        for ((j=1;j<=num_columns;j++)) do
            matrix[$i,$j]=0
        done
    done
    echo Klasyfikator $CLF
    i=0
    for file_name in *.txt; do
        ((i=i+1))
        echo -n $i" "
        line=`python ./../classify_in_folder.py --mode predict --data "./$file_name" --path ./../resources/pretrained_models/$CLF.bin --clf $CLF --epochs 100`
        read -a result <<< $line
        echo -n ${line}" "
        echo "$file_name"

        if [ "${result[0]}" == "${result[1]}" ]; then
            ((correct=correct+1))
        fi
        ((w=${result[0]}+2))
        ((c=${result[1]}+2))
        (( matrix[$w,$c]++ ))

    done
    echo -n Accuracy=
    echo "scale=2 ; $correct / $i" | bc
    echo "Macierz pomylek"
    for ((i=1;i<=num_rows;i++)) do
        for ((j=1;j<=num_columns;j++)) do
            echo -n ${matrix[$i,$j]}" "
        done
        echo
    done
    echo 
done

deactivate