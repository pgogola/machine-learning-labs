#include "../include/RlAgent.hpp"
#include <algorithm>
#include <iomanip>
#include <limits>
#include <random>
#include <stdexcept>

using Coordinates = RlAgent::Coordinates;
using Direction = World::Direction;

static std::random_device rd;
static std::default_random_engine generator(rd());
static std::uniform_real_distribution<double> distributionDouble(0, 1);
static std::uniform_int_distribution<int> distributionInt(0, 3);

static Coordinates nextPositionAfterAction(const Coordinates &state,
                                           const Direction &action,
                                           double *probabilitiesThresholds) {
  auto [row, col] = state;
  auto up = std::make_pair(row - 1, col);
  auto down = std::make_pair(row + 1, col);
  auto right = std::make_pair(row, col + 1);
  auto left = std::make_pair(row, col - 1);
  std::vector<std::pair<int, int>> dirs;
  switch (action) {
  case World::Direction::UP:
    dirs = std::move(std::vector{up, right, down, left});
    break;
  case World::Direction::DOWN:
    dirs = std::move(std::vector{down, left, up, right});
    break;
  case World::Direction::RIGHT:
    dirs = std::move(std::vector{right, down, left, up});
    break;
  case World::Direction::LEFT:
    dirs = std::move(std::vector{left, up, right, down});
    break;
  default:
    throw std::invalid_argument("invalid direction/action argument");
  }
  int i = 0;
  auto threshold = distributionDouble(generator);
  while (probabilitiesThresholds[i] <= threshold) {
    i++;
  }
  return dirs[i];
}

static Direction selectAction(const RlAgent::StateActionTab &q,
                              const Coordinates &state, double epsilon) {
  auto [currentRow, currentCol] = state;
  double probab = distributionDouble(generator);
  if (probab < epsilon) {
    return RlAgent::ACTION_ORDER[distributionInt(generator)];
  } else {
    return RlAgent::ACTION_ORDER[std::distance(
        std::begin(q.at(currentRow).at(currentCol)),
        std::max_element(std::begin(q.at(currentRow).at(currentCol)),
                         std::end(q.at(currentRow).at(currentCol))))];
  }
}

static std::vector<Coordinates> nextPositionSimple(const int row, const int col,
                                                   const Direction &action) {
  auto up = std::make_pair(row - 1, col);
  auto down = std::make_pair(row + 1, col);
  auto right = std::make_pair(row, col + 1);
  auto left = std::make_pair(row, col - 1);
  switch (action) {
  case World::Direction::UP:
    return std::vector{up, right, down, left};
  case World::Direction::DOWN:
    return std::vector{down, left, up, right};
  case World::Direction::RIGHT:
    return std::vector{right, down, left, up};
  case World::Direction::LEFT:
    return std::vector{left, up, right, down};
  default:
    throw std::invalid_argument("invalid direction/action argument");
  }
};

RlAgent::RlAgent(World &world, double epsilon)
    : IAgent(world), action{World::Direction::UNDEF},
      state{std::make_pair(3, 0)}, reward{0}, epsilon{epsilon} {
  int rows = world.getRows();
  int cols = world.getCols();
  for (int r = 0; r < rows; r++) {
    q.emplace_back(std::vector<std::vector<double>>());   // add row
    nsa.emplace_back(std::vector<std::vector<double>>()); // add row
    for (int c = 0; c < cols; c++) {
      q.at(r).emplace_back(std::vector<double>());   // add col
      nsa.at(r).emplace_back(std::vector<double>()); // add col
      utilities(r, c) = 0;
      for (int a = 0; a < 4; a++) {
        q.at(r).at(c).emplace_back(0);
        nsa.at(r).at(c).emplace_back(0);
      }
    }
  }
}

void RlAgent::iteration() {
  double probabilitiesThresholds[]{pForward, pForward + pRight,
                                   pForward + pRight + pBack,
                                   pForward + pRight + pBack + pLeft};

  auto alpha = [](int nsa) { return 1.0 / nsa; };
  const auto &scores = world.getScores();
  auto [prevR, prevC] = state;
  // select next action
  Direction action = selectAction(q, state, epsilon);
  auto positionAfterAction =
      nextPositionAfterAction(state, action, probabilitiesThresholds);
  // select next state with given probabilities
  auto nextState = fixPosition(state, positionAfterAction, world.getStates());
  auto [nextRow, nextCol] = nextState;
  auto qspaMax = *std::max_element(std::begin(q.at(nextRow).at(nextCol)),
                                   std::end(q.at(nextRow).at(nextCol)));
  double r = scores(nextRow, nextCol);
  nsa.at(prevR).at(prevC).at(action) += 1;
  q.at(prevR).at(prevC).at(action) +=
      alpha(nsa.at(prevR).at(prevC).at(action)) * // learning rate
      (r + gamma * qspaMax - q.at(prevR).at(prevC).at(action));
  state = nextState;
}

long long RlAgent::learn() {
  long long iterations = 0;
  const auto &states = world.getStates();
  state = world.getStartPosition();
  do {
    ++iterations;
    iteration();
  } while (World::State::T != states(state.first, state.second));
  return iterations;
}

void RlAgent::calculatePolicy() {
  int rows = world.getRows();
  int cols = world.getCols();
  double probabilities[]{pForward, pRight, pBack, pLeft};
  const auto utilityCalc = [this](const double *probabilities,
                                  const std::vector<Coordinates> &coordinates) {
    double actionUtility = 0;
    for (size_t i = 0; i < 4; i++) {
      auto [r, c] = coordinates[i];
      actionUtility += (probabilities[i] * utilities(r, c));
    }
    return actionUtility;
  };
  const auto &states = world.getStates();

  World::Direction directions[]{World::Direction::UP, World::Direction::DOWN,
                                World::Direction::RIGHT,
                                World::Direction::LEFT};

  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      if (World::State::F == states(row, col) ||
          World::State::T == states(row, col)) {
        policy(row, col) = World::directionIntChar[World::Direction::UNDEF];
      } else {
        double maxVal = -std::numeric_limits<double>::max();
        policy(row, col) = World::directionIntChar[World::Direction::UNDEF];
        for (int i = 0; i < 4; i++) {
          auto nextPositions = nextPositionSimple(row, col, directions[i]);
          for (int p = 0; p < 4; p++) {
            nextPositions[p] =
                fixPosition(std::make_pair(row, col), nextPositions[p], states);
          }
          double utility = utilityCalc(probabilities, nextPositions);
          if (utility > maxVal) {
            maxVal = utility;
            policy(row, col) = World::directionIntChar[directions[i]];
          }
        }
      }
    }
  }
}

std::string RlAgent::details() const {
  std::string result = IAgent::details() + "\n\n";
  int rows = world.getRows();
  int cols = world.getCols();

  std::stringstream ss;
  for (const auto &action : RlAgent::ACTION_ORDER) {
    ss << "Action: " << World::directionIntChar[action] << std::endl;
    for (int r = 0; r < rows; r++) {
      ss << "|";
      for (int c = 0; c < cols; c++) {
        ss << std::setw(10) << std::showpos << std::fixed
           << std::setprecision(4) << q.at(r).at(c).at(action);
        ss << "  | ";
      }
      ss << '\n';
    }
    ss << "--------------------------------------------\n";
  }
  result += ss.str();

  return result;
}

void RlAgent::updateUtility() {
  const auto &states = world.getStates();
  int rows = world.getRows();
  int cols = world.getCols();
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      if (World::State::T == states(row, col)) {
        utilities(row, col) = world.getScores()(row, col);
      } else {
        utilities(row, col) = *std::max_element(std::begin(q.at(row).at(col)),
                                                std::end(q.at(row).at(col)));
      }
    }
  }
}
