# Machine learning

## Laboratory 1

### Laboratory problems

1. Markov decision process (MDP)
2. Reinforcement learning (Q-algorithm)

### World representation (file format)

a) First line has ten numbers:

```bash
N M p1 p2 p3 r gamma N_T N_B N_F
```

first seven are MDP world parameters (*N, M* are integers, *p1 p2 p3 r gamma* are floating numbers, and *N_T N_B N_F* mean terminal states amount, special states amount, prohibited states respectively).

b) Next lines represents individual terminal, special and prohibited states. Amount of lines is *(N_T+N_B+N_F)*, and each has to start with symbol *T*, *B* or *F* followed by 3 numbers: two of them are position coordinates of state and the last one the prize. When state is *F* (prohibited) the last number is ignored.

Position starts at point *(1, 1)* from left bottom corner.

Parameters constraints:

```bash
N, M > 0
p1, p2, p3 >= 0.0 <= 1.0
p1+p2+p3 <= 1.0
gamma > 0.0 <= 1.0
N_T > 0
N_B >= 0
N_F >= 0
```

#### Example file from Russell and Norvig file

```bash
4 3 .8 .1 .1 -0.04 1.00 2 0 1
T 4 3 1.
T 4 2 -1.
F 2 2 0.0
```

### Examples

#### Case 1

```bash
| | | |T|
| |F| |T|
|S| | | |

T=1, T=-1, gamma=1.0, r=-0.4, p1=0.8,  p2=0.1, p3=0.1
```

#### Case 2

```bash
| | | | |
| | | | |
| | |B| |
|S| |F|T|

T=100, B=-20, gamma=0.99, r=-1, p1=0.8,  p2=0.1, p3=0.1
```

#### Case 3 (base case 2 - other reward values)

```bash
T=100, B=-20, gamma=0.99, r=-1, p1=0.8,  p2=0.1, p3=0.1
```

#### Case 4 (base case 2 - other movement probability model)

```bash
T=100, B=-20, gamma=0.99, r=-1, p1=0.8,  p2=0.1, p3=0.1
```

#### Case 5 (base case 2 - other gamma discount coefficient value)

```bash
T=100, B=-20, gamma=0.99, r=-1, p1=0.8,  p2=0.1, p3=0.1
```

## Laboratory 2

### Laboratory problems

1. Brexit articles classification: anti-brexit, neutral, pro-brexit with different machine learning approaches

[Laboratory 2 Readme (only polish version)](lab2/Readme.md)
