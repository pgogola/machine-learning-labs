#!/bin/bash

source ./env/bin/activate

for CLF in "zeror" "gnb" "gnbova" "mnb" "mnbova" "svm" "svmova" "nn" 
do
python ./main.py --mode train --data ./resources/train.txt --path ./resources/pretrained_models/$CLF.bin --clf $CLF --epochs 100
done
deactivate