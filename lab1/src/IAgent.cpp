#include "../include/IAgent.hpp"
#include "../include/World.hpp"
#include <iomanip>
#include <iostream>

std::pair<int, int> IAgent::fixPosition(std::pair<int, int> currentPosition,
                                        std::pair<int, int> nextPosition,
                                        WorldArr<World::State> states) {
  int rows = states.getRows();
  int cols = states.getCols();
  auto [r, c] = nextPosition;
  if (r < 0 || c < 0 || r >= rows || c >= cols ||
      World::State::F == states(r, c)) {
    return currentPosition;
  }
  return nextPosition;
};

WorldArr<std::list<double>> IAgent::getHistory() const { return history; }

IAgent::IAgent(World &world)
    : world{world}, aware{0}, gamma{world.getGamma()}, r{world.getR()},
      pForward{world.getP1()}, pRight{world.getP2()}, pLeft{world.getP3()},
      pBack(1 - world.getP1() - world.getP2() - world.getP3()),
      history{std::move(
          WorldArr<std::list<double>>(world.getRows(), world.getCols()))},
      utilities{std::move(WorldArr<double>(world.getRows(), world.getCols()))},
      policy{std::move(WorldArr<char>(world.getRows(), world.getCols()))} {}

std::string IAgent::details() const {
  std::string result;

  int rows = world.getRows();
  int cols = world.getCols();
  for (int r = 0; r < rows; r++) {
    result += "|  ";
    for (int c = 0; c < cols; c++) {
      result += policy(r, c);
      result += "  |  ";
    }
    result += '\n';
  }
  result += "\n\n";

  std::stringstream ss;
  for (int r = 0; r < rows; r++) {
    ss << "|";
    for (int c = 0; c < cols; c++) {
      ss << std::setw(10) << std::showpos << std::fixed << std::setprecision(4)
         << utilities(r, c);
      ss << "  | ";
    }
    ss << '\n';
  }
  result += ss.str();
  return result;
}