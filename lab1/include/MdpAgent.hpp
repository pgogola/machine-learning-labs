#pragma once

#include "IAgent.hpp"
#include "World.hpp"

class MdpAgent : public IAgent {
public:
  explicit MdpAgent(World &world);
  virtual void iteration() override;
  virtual long long learn() override;
  virtual ~MdpAgent() = default;
  virtual void calculatePolicy() override;
  // virtual std::string details() const override;
};