#include "../include/World.hpp"
#include <fstream>
#include <iostream>
#include <unistd.h>

std::ostream &operator<<(std::ostream &os, const World::State &obj) {
  os << static_cast<std::underlying_type<World::State>::type>(obj);
  return os;
}

std::ostream &operator<<(std::ostream &os, const World::Direction &obj) {
  os << static_cast<std::underlying_type<World::Direction>::type>(obj);
  return os;
}

World World::readWorldFromFile(const std::string &filename) {
  std::ifstream file(filename);
  if (!file.is_open()) {
    throw "File does not exists";
  }
  int cols, rows, n_t, n_b, n_f;
  double p1, p2, p3, r, gamma;
  file >> cols >> rows >> p1 >> p2 >> p3 >> r >> gamma >> n_t >> n_b >> n_f;
  int col, row;
  double val;
  char state;
  World world(rows, cols);
  world.setR(r);
  world.setGamma(gamma);
  world.setN_b(n_b);
  world.setN_f(n_f);
  world.setN_t(n_t);
  world.setP1(p1);
  world.setP2(p2);
  world.setP3(p3);
  auto &scores = world.getScores();
  auto &states = world.getStates();
  while (file >> state >> col >> row >> val) {
    row = rows - row;
    col -= 1;
    scores(row, col) = val;
    states(row, col) = static_cast<World::State>(state);
  }
  return world;
}

World::World(int rows, int cols)
    : rows{rows}, cols{cols}, p1{0}, p2{0}, p3{0}, r{0}, gamma{0}, n_t{0},
      n_b{0}, n_f{0}, scores{WorldArr<double>(rows, cols)},
      directions{WorldArr<Direction>(rows, cols)},
      states{WorldArr<State>(rows, cols)}, startPosition{std::make_pair(1, 1)} {
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      this->scores(i, j) = 0;
      this->directions(i, j) = Direction::UNDEF;
      this->states(i, j) = State::N;
    }
  }
  setStartPosition(rows - 1, 0);
}

World::World(const World &other)
    : rows{other.rows}, cols{other.cols}, p1{other.p1}, p2{other.p2},
      p3{other.p3}, r{other.r}, gamma{other.gamma}, n_t{other.n_t},
      n_b{other.n_b}, n_f{other.n_f}, scores{other.scores},
      directions{other.directions}, states{other.states},
      startPosition{other.startPosition} {}

World::World(World &&other)
    : rows{other.rows}, cols{other.cols}, p1{other.p1}, p2{other.p2},
      p3{other.p3}, r{other.r}, gamma{other.gamma}, n_t{other.n_t},
      n_b{other.n_b}, n_f{other.n_f}, scores{std::move(other.scores)},
      directions{std::move(other.directions)}, states{std::move(other.states)},
      startPosition{std::move(other.startPosition)} {}

World::~World() {}

const std::pair<int, int> &World::getStartPosition() const {
  return this->startPosition;
}

void World::setStartPosition(int row, int col) {
  this->states(startPosition.first, startPosition.second) = State::N;
  this->startPosition.first = row;
  this->startPosition.second = col;
  this->states(row, col) = State::S;
}

const WorldArr<double> &World::getScores() const { return scores; }
WorldArr<double> &World::getScores() { return scores; }
const WorldArr<World::State> &World::getStates() const { return states; }
WorldArr<World::State> &World::getStates() { return states; }
const WorldArr<World::Direction> &World::getDirections() const {
  return directions;
}
WorldArr<World::Direction> &World::getDirections() { return directions; }