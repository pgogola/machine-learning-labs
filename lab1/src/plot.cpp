#include "../include/plot.hpp"
#include <fstream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

FileManager::FileManager(const std::string &outFileName)
    : outFileName{outFileName}, fileHandler{nullptr}, it{0} {}

bool FileManager::open() {
  fileHandler = std::make_unique<std::fstream>(outFileName, std::ios::out);
  return fileHandler->is_open();
}

bool FileManager::close() {
  fileHandler->close();
  return !fileHandler->is_open();
}

bool FileManager::prepare(const WorldArr<double> &utility) {
  const auto rows = utility.getRows();
  const auto cols = utility.getCols();
  it = 1;
  try {
    *fileHandler << "lp ";
    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < cols; c++) {
        *fileHandler << "(" << (c + 1) << "," << (rows - r) << ") ";
      }
    }
    *fileHandler << "\n";
  } catch (...) {
    return false;
  }
  return true;
}
bool FileManager::writeData(const WorldArr<double> &utility) {
  const auto rows = utility.getRows();
  const auto cols = utility.getCols();
  try {
    *fileHandler << it << " ";
    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < cols; c++) {
        *fileHandler << utility(r, c) << " ";
      }
    }
    *fileHandler << "\n";
    it++;
  } catch (...) {
    return false;
  }
  return true;
}

void saveGraphUtilityHistory(const WorldArr<std::list<double>> &history,
                             const std::string &outFileName) {
  std::fstream outF(outFileName, std::ios::out);
  if (!outF.is_open()) {
  }
  const auto rows = history.getRows();
  const auto cols = history.getCols();
  WorldArr<std::list<double>::const_iterator> historyIt(rows, cols);
  outF << "lp ";
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      historyIt(r, c) = std::begin(history(r, c));
      outF << "(" << (c + 1) << "," << (rows - r) << ") ";
    }
  }
  bool isAnyTrue;
  unsigned int i = 0;
  do {
    i++;
    outF << std::endl << i << " ";
    isAnyTrue = false;
    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < cols; c++) {
        if (historyIt(r, c) != std::end(history(r, c))) {
          outF << *(historyIt(r, c)++) << " ";
          isAnyTrue = true;
        } else {
          outF << history(r, c).back() << " ";
        }
      }
    }
  } while (isAnyTrue);

  outF.close();
}

void plotGraphFromFile(const std::string &graphFileName) {
  std::fstream graphFile(graphFileName);
  std::string firstLine;
  std::getline(graphFile, firstLine);
  graphFile.close();
  std::istringstream iss(firstLine);
  std::vector<std::string> header((std::istream_iterator<std::string>(iss)),
                                  std::istream_iterator<std::string>());

  FILE *gplot = popen("gnuplot", "w");
  fprintf(gplot, "set offsets 0.0, 0.0, 0.1, 0.1\n");
  fprintf(gplot, "set key outside\n");
  fprintf(gplot, "set terminal png\n");
  fprintf(gplot, "set output '%s.png'\n", graphFileName.c_str());
  /*fprintf(gplot, "set palette defined (\
    1  '#0025ad', \
    2  '#0042ad', \
    3  '#0060ad', \
    4  '#007cad', \
    5  '#0099ad', \
    6  '#00ada4', \
    7  '#00ad88', \
    8  '#00ad6b', \
    9  '#00ad4e', \
    10 '#00ad31', \
    11 '#00ad14', \
    12 '#09ad00', \
    13 '#00ad15', \
    14 '#00ad16', \
    15 '#00ad17', \
    16 '#00ad18', \
    17 '#00ad19', \
    18 '#00ad20', \
    19 '#00ad21', \
    20 '#00ad22', \
    21 '#00ad23', \
    22 '#00ad24', \
    23 '#00ad25', \
    24 '#00ad26', \
    25 '#00ad27', \
    26 '#00ad28' \
    )\n");*/
  fprintf(gplot, "plot ");
  for (size_t i = 2; i < header.size(); i++) {
    fprintf(gplot, "\"%s\" u 1:%lu with lines title \"%s\",\\\n",
            graphFileName.c_str(), i, header.at(i - 1).c_str());
  }
  fprintf(gplot, "\"%s\" u 1:%lu with lines title \"%s\",\n",
          graphFileName.c_str(), header.size(), header.back().c_str());
  fflush(gplot);
  // std::cin.get();
  pclose(gplot);
}