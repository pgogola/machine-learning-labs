
#pragma once

#include "IAgent.hpp"
#include <unordered_map>
#include <vector>

class RlAgent : public IAgent {
public:
  struct TupleKeyHash;
  using TupleKey = std::tuple<int, int, World::Direction>;
  using Coordinates = std::pair<int, int>;
  using StateActionTab = std::vector<std::vector<std::vector<double>>>;

  explicit RlAgent(World &world, double epsilon);
  virtual void iteration() override;
  virtual long long learn() override;
  virtual ~RlAgent() = default;
  virtual void calculatePolicy() override;
  virtual std::string details() const override;
  void updateUtility();

  struct TupleKeyHash : public std::unary_function<TupleKey, std::size_t> {
    std::size_t operator()(const TupleKey &key) const {
      return std::get<0>(key) ^ std::get<1>(key) ^ std::get<2>(key);
    }
  };
  static constexpr World::Direction ACTION_ORDER[4]{
      World::Direction::UP, World::Direction::RIGHT, World::Direction::DOWN,
      World::Direction::LEFT};

private:
  World::Direction action;
  Coordinates state;
  double reward;
  double epsilon;
  StateActionTab nsa;
  StateActionTab q;
};