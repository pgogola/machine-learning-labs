\babel@toc {polish}{}
\contentsline {section}{\numberline {1}Problem decyzyjny Markowa -- iteracja warto\IeC {\'s}ci}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}\IeC {\'S}wiat 4x3 z wyk\IeC {\l }adu (bez dyskontowania)}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}\IeC {\'S}wiat 4x4 -- podstawowy}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}\IeC {\'S}wiat 4x4 -- zmieniona funkcja nagrody}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}\IeC {\'S}wiat 4x4 -- zmieniony model niepewno\IeC {\'s}ci}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}\IeC {\'S}wiat 4x4 -- zmieniony wsp\IeC {\'o}\IeC {\l }czynnik dyskontowania}{5}{subsection.1.5}
\contentsline {section}{\numberline {2}Uczenie ze wzmocnieniem -- Q-learning}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}\IeC {\'S}wiat 4x4 -- $\varepsilon {}=0.05$}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}\IeC {\'S}wiat 4x4 -- $\varepsilon {}=0.2$}{10}{subsection.2.2}
