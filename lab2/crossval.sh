#!/bin/bash

source ./env/bin/activate

for CLF in "zeror" "gnb" "gnbova" "mnb" "mnbova" "svm" "svmova" "nn"
do
python ./main.py --mode crossval -k 10 --data ./resources/dataset.txt --path ./tmp/model.bin --epochs 100 --clf $CLF --embed_dim 32
done
deactivate