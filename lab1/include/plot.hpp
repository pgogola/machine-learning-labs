#pragma once

#include "World.hpp"
#include <fstream>
#include <list>
#include <memory>

class FileManager {
public:
  explicit FileManager(const std::string &outFileName);
  FileManager(const FileManager &other) = delete;
  FileManager(const FileManager &&other) = delete;

  bool open();
  bool close();
  bool prepare(const WorldArr<double> &utility);
  bool writeData(const WorldArr<double> &utility);

private:
  std::string outFileName;
  std::unique_ptr<std::fstream> fileHandler;
  size_t it;
};

void saveGraphUtilityHistory(const WorldArr<std::list<double>> &history,
                             const std::string &outFileName);

void plotGraphFromFile(const std::string &graphFileName);