#include "../include/MdpAgent.hpp"
#include <algorithm>
#include <iomanip>
#include <limits>
#include <random>
#include <stdexcept>

using Direction = World::Direction;
using Coordinates = std::pair<int, int>;

static bool allSmallerThan(const WorldArr<double> &current,
                           const WorldArr<double> &prev, double epsilon) {
  for (int i = 0; i < current.getRows(); i++) {
    for (int j = 0; j < current.getCols(); j++) {
      if (std::abs(current(i, j) - prev(i, j)) > epsilon) {
        return false;
      }
    }
  }
  return true;
}

static std::vector<Coordinates>
nextPositionAfterAction(const int row, const int col, const Direction &action) {
  auto up = std::make_pair(row - 1, col);
  auto down = std::make_pair(row + 1, col);
  auto right = std::make_pair(row, col + 1);
  auto left = std::make_pair(row, col - 1);
  switch (action) {
  case World::Direction::UP:
    return std::vector{up, right, down, left};
  case World::Direction::DOWN:
    return std::vector{down, left, up, right};
  case World::Direction::RIGHT:
    return std::vector{right, down, left, up};
  case World::Direction::LEFT:
    return std::vector{left, up, right, down};
  default:
    throw std::invalid_argument("invalid direction/action argument");
  }
};

MdpAgent::MdpAgent(World &world) : IAgent(world) {
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);
  auto rows = world.getRows();
  auto cols = world.getCols();
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      if (World::State::T != world.getStates()(i, j)) {
        utilities(i, j) = 0;
      } else {
        utilities(i, j) = world.getScores()(i, j);
      }
    }
  }
}

void MdpAgent::iteration() {
  auto rows = world.getRows();
  auto cols = world.getCols();
  World::Direction actions[]{World::Direction::UP, World::Direction::DOWN,
                             World::Direction::LEFT, World::Direction::RIGHT};
  double probabilities[]{pForward, pRight, pBack, pLeft};
  auto states = world.getStates();
  const auto prevUtilities = utilities;
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      if (World::State::T == states(i, j) || World::State::F == states(i, j)) {
        // skip if state is forbidden
        continue;
      }
      auto currentPosition = std::make_pair(i, j);
      double maxUtility = -std::numeric_limits<double>::max();
      for (auto action = std::begin(actions); action != std::end(actions);
           action++) {
        auto positions = nextPositionAfterAction(i, j, *action);
        double currentActionUtility = 0;
        for (int pos = 0; pos < 4; pos++) {
          auto [nextR, nextC] =
              fixPosition(currentPosition, positions.at(pos), states);
          currentActionUtility +=
              probabilities[pos] * prevUtilities(nextR, nextC);
        }
        maxUtility = std::max(maxUtility, currentActionUtility);
      }
      utilities(i, j) = world.getScores()(i, j) + gamma * maxUtility;
    }
  }
}

long long MdpAgent::learn() {
  long long iterations = 0;
  WorldArr<double> prevUtilities = getUtilities();
  const auto rows = world.getRows();
  const auto cols = world.getCols();
  do {
    prevUtilities = getUtilities();
    iteration();
    ++iterations;
    const auto &currentUtilities = getUtilities();
    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < cols; c++) {
        history(r, c).emplace_back(currentUtilities(r, c));
      }
    }
  } while (!allSmallerThan(getUtilities(), prevUtilities, 0.0001));
  return iterations;
}

void MdpAgent::calculatePolicy() {
  int rows = world.getRows();
  int cols = world.getCols();
  double probabilities[]{pForward, pRight, pBack, pLeft};
  const auto utilityCalc = [this](const double *probabilities,
                                  const std::vector<Coordinates> &coordinates) {
    double actionUtility = 0;
    const auto &utilities = getUtilities();
    for (size_t i = 0; i < 4; i++) {
      auto [r, c] = coordinates[i];
      actionUtility += (probabilities[i] * utilities(r, c));
    }
    return actionUtility;
  };
  const auto &states = world.getStates();
  World::Direction directions[]{World::Direction::UP, World::Direction::DOWN,
                                World::Direction::RIGHT,
                                World::Direction::LEFT};

  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      if (World::State::F == states(row, col) ||
          World::State::T == states(row, col)) {
        policy(row, col) = World::directionIntChar[World::Direction::UNDEF];
      } else {
        double maxVal = -std::numeric_limits<double>::max();
        policy(row, col) = World::directionIntChar[World::Direction::UNDEF];
        for (int i = 0; i < 4; i++) {
          auto nextPositions = nextPositionAfterAction(row, col, directions[i]);
          for (int p = 0; p < 4; p++) {
            nextPositions[p] =
                fixPosition(std::make_pair(row, col), nextPositions[p], states);
          }

          double utility = utilityCalc(probabilities, nextPositions);
          if (utility > maxVal) {
            maxVal = utility;
            policy(row, col) = World::directionIntChar[directions[i]];
          }
        }
      }
    }
  }
}