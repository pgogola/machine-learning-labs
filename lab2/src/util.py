import re
import numpy as np
import random
from sklearn.metrics import precision_score, recall_score


def txt_data_loader(file_name):
    """Function which loads data from txt file

    Arguments:
        file_name {string} -- data file name

    Returns:
        list -- list of pairs (label, string)
    """
    data = []
    with open(file_name, 'r') as f:
        for line in f:
            splitted = line.split("\t")
            data.append((int(splitted[0]), splitted[1]))
    return data


def string_to_words(text):
    """Get list of words from string. Remove all non-alphanumeric characters.

    Arguments:
        text {str} -- text

    Returns:
        list -- list of words
    """
    res = re.findall(r'\w+', text)
    return [s.lower() for s in res]


def get_all_words_set(data):
    """Get set of words in dataset

    Arguments:
        data {list} -- list of tuples (class, text)

    Returns:
        set -- set of words
    """
    word_set = set()
    for c, s in data:
        words = string_to_words(s)
        for w in words:
            word_set.add(w)
    return word_set


def data_to_bow(data):
    """Convert data to bag-of-words

    Arguments:
        data {list} -- list of tuples (class, text)

    Returns:
        list -- list of tuples (class, dictionary(word:word occurances count))
    """
    dataset = []
    for c, s in data:
        words = string_to_words(s)
        bag = dict()
        for w in words:
            if not w in bag:
                bag[w] = 0
            bag[w] += 1
        dataset.append((c, bag))
    return dataset


def data_to_idfm(data, word_encoding=None):
    """Convert data to document-term frequency matrix

    Arguments:
        data {list} -- list of tuples (class, text)

    Returns:
        list -- list of tuples (class, words frequencies)
    """
    dfm = []
    if word_encoding is None:
        words = get_all_words_set(data)
        words_amount = len(words)
        word_encoding = dict()
        for i, w in enumerate(words):
            word_encoding[w] = i
    else:
        words_amount = len(word_encoding)
    bow = data_to_bow(data)
    for c, bow_words in bow:
        text_frec = np.zeros(words_amount)
        words_in_text = np.sum([a for a in bow_words.values()])
        for word, amount in bow_words.items():
            if word in word_encoding:
                text_frec[word_encoding[word]] = amount/words_in_text
        dfm.append((c, text_frec))
    return dfm, word_encoding


def data_to_dfm(data, word_encoding=None):
    """Convert data to document-term frequency matrix

    Arguments:
        data {list} -- list of tuples (class, text)

    Returns:
        list -- list of tuples (class, words count)
    """
    dfm = []
    if word_encoding is None:
        words = get_all_words_set(data)
        words_amount = len(words)
        word_encoding = dict()
        for i, w in enumerate(words):
            word_encoding[w] = i
    else:
        words_amount = len(word_encoding)
    bow = data_to_bow(data)
    for c, bow_words in bow:
        text_frec = np.zeros(words_amount)
        for word, amount in bow_words.items():
            if word in word_encoding:
                text_frec[word_encoding[word]] = int(amount)
        dfm.append((c, text_frec))
    return dfm, word_encoding


def divide_set(dataset, sep):
    """Divide given set into two sets

    Arguments:
        dataset {list} -- list with data samples
        sep {float} -- number between 0.0-1.0

    Returns:
        (list, list) -- two lists
    """
    dataset_size = len(dataset)
    separator_idx = int(dataset_size*sep)
    return dataset[:separator_idx], dataset[separator_idx:]


def data_for_clf_np(data):
    """Prepare data for classifier

    Arguments:
        data {list} -- list of tuples (class, numpy.array)

    Returns:
        (numpy.array, numpy.array) -- numpy array with samples and numpy array with labels
    """
    labels = []
    samples = []
    for c, t in data:
        labels.append(c)
        samples.append(t)

    return np.vstack(samples), np.array(labels)


def get_ngrams(text, ngram_size):
    """Generate n-grams from text

    Arguments:
        text {str} -- text to generate ngrams from
        ngram_size {int} -- number of words in ngram

    Returns:
        list -- list of strings
    """
    words = string_to_words(text)
    len_words = len(words)
    ngrams = []
    for i in range(0, len_words-ngram_size+1):
        ngrams.append(" ".join(words[i:i+ngram_size]))
    return ngrams


def print_table_latex(tab):
    classes = [-1, 0, 1]
    row_name = "sklasyfikowana klasa: "

    result = ""

    for (i, row) in enumerate(tab):
        result += (row_name + str(classes[i]))
        for c in row:
            result += (" & " + str(int(c)))
        result += (" \\\\\n\\hline\n")
    return result


def calc_precision(y_pred, y_true):
    return round(precision_score(y_true, y_pred, average='micro'), 2)


def calc_recall(y_pred, y_true):
    return round(recall_score(y_true, y_pred, average='micro'), 2)


def shuffle_data(data):
    random.shuffle(data)
    return data


if __name__ == "__main__":
    data = txt_data_loader('./resources/dataset.txt')
    print(data[0][1])
    print(get_ngrams(data[0][1], 2))
